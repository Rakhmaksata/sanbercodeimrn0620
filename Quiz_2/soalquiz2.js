//Soal 1
class Score {
    constructor(subject,points,email) {
        this.subject = subject
        this.points = points
        this.email = email
    }
    
    nilai(){
    	if(Array.isArray(this.points)){
    		var sum = 0;
		for( var i = 0; i < this.points.length; i++ ){
		    sum += parseInt( this.points[i], 10 ); 

		var avg = sum/this.points.length;
    		return avg;
    	}
    	else {
    		return this.points;
    	}
    }
}

nscore = new Score('quiz 1',[10,8,8],'a@a.com');
console.log(nscore.nilai());

//Soal 2
const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  for (i = 1; i < data.length; i++) {
    var dataScore = new Score(data[i][0], subject, data[i].slice(1, 4))
    console.log(dataScore)
  }
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


//Soal 3
function recapScores(data) {
  var arrIsi = data.slice(1,data.length);
  for (var i=0; i < arrIsi.length; i++){
  	var nilai = (arrIsi[i][1]+arrIsi[i][2]+arrIsi[i][3])/3;
	var predikat = '';
	if(nilai > 90){
		predikat = 'honour';
	} else if(nilai > 80){
		predikat = 'graduate';
	} else if(nilai > 70){
		predikat = 'participant';
	}
  	console.log(i+1 +'. Email: '+arrIsi[i][0]);
  	console.log('Rata-rata: ' + nilai.toFixed(1));
  	console.log('Predikat: ' + predikat +'\n');
  }
  
}

recapScores(data);
