//Soal A
function bandingkan(num1, num2)
{
	if (num1 < 0 || num2 < 0 || num1 == num2 || num2 == undefined)
		return -1
	else if (num1 > num2)
		return num1
	else
		return num2
}
console.log(bandingkan(10, 15)); 
console.log(bandingkan(12, 12)); 
console.log(bandingkan(-1, 10)); 
console.log(bandingkan(112, 121));
console.log(bandingkan(1)); 
console.log(bandingkan()); 
console.log(bandingkan("15", "18"))

//Soal B
function balikString(kata="") 
{
    var balik = ""
    for (let a = kata.length-1; a >= 0; a--) 
    {
        balik += kata[a]
    }
    return balik
}
console.log(balikString("abcde")) 
console.log(balikString("rusak")) 
console.log(balikString("racecar")) 
console.log(balikString("haji")) 

//Soal C
function palindrome(kata) 
{
  for(var a = 0; a < kata.length/2; a++)
  {
    if(kata[a]!=kata[kata.length-1-a])
      return false;
  }
  return true;

}
console.log(palindrome("kasur rusak")) 
console.log(palindrome("haji ijah")) 
console.log(palindrome("nabasan")) 
console.log(palindrome("nababan")) 
console.log(palindrome("jakarta")) 
