//Soal A
function DescendingTen(angka) 
{
    if (angka == undefined) 
    {
        return -1
    }
    var deret = []
    for (let a = 0; a < 10; a++) 
    {
        deret.push(`${angka-a}`)
    }
    var urut = deret.join(" ")
    return urut
}
console.log(DescendingTen(100)) 
console.log(DescendingTen(10)) 
console.log(DescendingTen()) 

//Soal B
function AscendingTen(angka) 
{
    if (angka == undefined) 
    {
        return -1
    }
    var deret = []
    for (let a = 0; a < 10; a++) 
    {
        deret.push(`${angka+a}`)
    }
    var urut = deret.join(' ')
    return urut
}
console.log(AscendingTen(11)) 
console.log(AscendingTen(21)) 
console.log(AscendingTen()) 

//Soal C
function ConditionalAscDesc(reference, check) 
{
	if(typeof reference==="undefined" || typeof check==="undefined") 
		return -1;
	else if(check%2>0) 
		return AscendingTen(reference);
	else 
		return DescendingTen(reference);
}
console.log(ConditionalAscDesc(20, 8)) 
console.log(ConditionalAscDesc(81, 1)) 
console.log(ConditionalAscDesc(31)) 
console.log(ConditionalAscDesc()) 

//Soal D
function ularTangga() 
{
	var output=""
	for(var i=100;i>=10;i-=10)
	{
		if(i%20==0) 
			output+=DescendingTen(i)+"\n";
		else 
			output+=AscendingTen(i-9)+"\n";
	}
	return output;
}
console.log(ularTangga())