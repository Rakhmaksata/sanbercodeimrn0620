//Soal 1
function range(startNum, finishNum)
{
    var result = []
    if (startNum == undefined || finishNum == undefined)
    {
        return -1
    }
    else if (startNum > finishNum)
    {
        for (var index = startNum; index >= finishNum; index--)
            result.push(index);
    }
    else
    {
        for (var index = startNum; index <= finishNum; index++)
            result.push(index);
    }
    return result
}
console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range())

//Soal 2
function rangeWithStep(startNum, finishNum, step)
{
    var result = []
    if (startNum == undefined || finishNum == undefined || step == undefined)
    {
        return -1
    }
    else if (startNum > finishNum)
    {
        for (var index = startNum; index >= finishNum; index -= step)
            result.push(index);
    }
    else
    {
        for (var index = startNum; index <= finishNum; index += step)
            result.push(index);
    }
    return result
}
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))

//Soal 3  
function sum (startSum, finishSum, step)
{
    var result = 0
    if (startSum == undefined)
        return 0
    else if (finishSum == undefined)
        return startSum
    else if (step == undefined)
        step = 1

    if (startSum > finishSum)
    {
        for (var index = startSum; index >= finishSum; index -= step)
            result += index
    }
    else
    {
        for (var index = startSum; index <= finishSum; index += step)
            result += index
    }
    return result
}
console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum()) 

//Soal 4
function dataHandling(data)
{
    var tampil = ""
    var text = ["Nomor ID : ","Nama Lengkap : ","TTL : ", "Hobi : "]

    for (var a = 0; a < data.length; a++) {
    
        panjang = data[a].length -1
        for (var b = 0; b < panjang ; b++) {
            if(b==3) {
                tampil += text[b] + data[a][b+1] + '\n\n'
            } else if (b==2) {
                tampil += text[b] + data[a][b] +' '+ data[a][b+1] + '\n'
            } else {
                tampil += text[b] + data[a][b] + '\n'
            }
        }
    
    }
    return tampil
}
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
console.log (dataHandling(input))

//Soal 5
function balikKata(input) {
    var balik =""
    for (var kata = input.length; kata >= 0; kata--) 
        {
            balik += input.charAt(kata)
        }
    return balik
}
console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 

//Soal 6
function dataHandling2(data)
{   
    data[1]+=" Elsharawy";
    data[2]= "Provinsi " + data[2];
    data.splice(4,1,"Pria","SMA Internasional Metro");
    console.log(data);
    var date=data[3].split("/");
    switch(Number(date[1]))
    {
        case 1: 
        { 
            var namabulan = "Januari"; 
            break; 
        }
        case 2: 
        { 
            var namabulan = "Februari"; 
            break; 
        }
        case 3: 
        { 
            var namabulan = "Maret"; 
            break; 
        }
        case 4: 
        { 
            var namabulan = "April"; 
            break; 
        }
        case 5: 
        { 
            var namabulan = "Mei"; 
            break; 
        }
        case 6: 
        { 
            var namabulan = "Juni"; 
            break; 
        }
        case 7: 
        { 
            var namabulan = "Juli"; break; 
        }
        case 8: 
        { 
            var namabulan = "Agustus"; break; 
        }
        case 9: 
        { 
            var namabulan = "September"; 
            break; 
        }
        case 10: 
        { 
            var namabulan = "Oktober"; 
            break; 
        }
        case 11: 
        { 
            var namabulan = "November"; 
            break; 
        }
        case 12: 
        { 
            var namabulan = "Desember"; 
            break; 
        }
    }
    console.log(namabulan);
    date.sort(function(a, b){return b - a});
    console.log(date);
    console.log((data[3].split("/")).join("-"));
    console.log(data[1].slice(0,14));

}
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
