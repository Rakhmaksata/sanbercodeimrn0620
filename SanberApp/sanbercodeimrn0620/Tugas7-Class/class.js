//Soal 1 
//Release 0
class Animal 
{
   constructor(name) 
   {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
class Ape extends Animal
{
	constructor(name)
	{
		super(name)
		this.legs = 2
	}
	yell()
	{
		console.log("Auooo");
	}
}
class Frog extends Animal
{
	constructor(name)
	{
		super(name)
	}
	jump()
	{
		console.log("hop hop");
	}
}

var sungokong = new Ape("kera sakti");
console.log (sungokong.name);
sungokong.yell() 

var kodok = new Frog("buduk")
console.log (kodok.name);
kodok.jump()

console.log ("\n");

//Soal 2
class Clock{
	constructor(template)
	{
		this.template = template
		this.timer = undefined
	}
    render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = hours+":"+mins+":"+secs;

    console.log(output);
  	}
  	stop() {
	    clearInterval(template);
	  };

	start(){
	    this.timer = setInterval(this.render, 1000);
	  };
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
