//Soal 1
console.log ("LOOPING PERTAMA");
var tata = 2;
while (tata <= 20)
{
	console.log (tata + " - I love coding");
	tata+=2;
}

console.log ("LOOPING KEDUA");
var tata = 20;
while (tata > 1)
{
	console.log (tata + " - I will become a mobile developer");
	tata-=2;
}

//Soal 2
var tata1 = 1;
for (tata1 = 1; tata1 <=20; tata1++)
{
	if (tata1 % 2 == 1)
		{
			if (tata1 % 3 ==0)
				console.log (tata1 + " I Love Coding");
			else
				console.log (tata1 + " Santai");
		}	
	else if (tata1 % 2 == 0)
		console.log (tata1 + " Berkualitas");		
	else
		console.log ("Salah ya")
}

//Soal 3
for (var i = 1; i <= 4; i++)
{
	for (var j = 1; j <= 8; j++)
	{
		process.stdout.write ("#"); //without trailing newline
	}
	console.log (" ");
}

//Soal 4
var text = "#"
for (var i = 0; i < 7; i++)
{
	for (var j = 0; j <= i; j++)
	{
		process.stdout.write (text);
	}
	console.log (" ");
}

//Soal 5
var board  = "";
var rows = 0;

while(rows < 8) {
    var cols = 0;
    var previousHashed;

    if(rows % 2 === 0 ) {
        previousHashed = true
    } else {
        previousHashed = false;
    }

    while(cols < 8) {
        if(previousHashed) {
            board += ' ';
        } else {
            board += '#';
        }

        previousHashed = !previousHashed;
        
        cols++;
    }

    board += "\n";

    rows++;
}

console.log(board);
