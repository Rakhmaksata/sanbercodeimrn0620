//Soal 1
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() 
    
    var obj={}
    for(var i=0;i<arr.length;i++){
        obj[(i+1)+". "+arr[i][0]+' '+arr[i][1]]={
            firstName:arr[i][0],
            lastName:arr[i][1],
            gender:arr[i][2],
            age: (typeof arr[i][3]==="undefined" || arr[i][3]>thisYear)?"Invalid Birth Year":thisYear-arr[i][3]
        }
    }
    console.log(obj);
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([]) 

//Soal 2
function shoppingTime(memberId, money) {
    if (typeof memberId == "undefined" || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    if (typeof money == "undefined" || money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }
    var item = [{
            name: "Sepatu Stacattu",
            price: 1500000
        },
        {
            name: "Baju Zoro",
            price: 500000
        },
        {
            name: "Baju H&N",
            price: 250000
        },
        {
            name: "Sweater Uniklooh",
            price: 175000
        },
        {
            name: "Casing Handphone",
            price: 50000
        }
    ];
    var data = {};
    data.memberId = memberId;
    data.money = money;

    var listPurchased = [];
    var changeMoney = 0;
    for (var i = 0; i < item.length; i++) {
        if (money >= item[i]["price"]) {
            listPurchased.push(item[i]["name"]);
            money -= item[i]["price"];
        }
        changeMoney = money;
    }
    data.listPurchased = listPurchased;
    data.changeMoney = changeMoney;
    return data;
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 

//Soal 3
function naikAngkot(arrPenumpang=[[]]) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrResult = []
    for (let i = 0; i < arrPenumpang.length; i++) {
        var strRute = rute.join("");
        var ruteCounter = Number(strRute.indexOf(arrPenumpang[i][2])) - Number(strRute.indexOf(arrPenumpang[i][1]))
        var bayar = ruteCounter*2000
        var penumpang = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: bayar
        }
        arrResult.push(penumpang)
    }
    return arrResult
}
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));